import org.nexa.libnexakotlin.*
import kotlin.test.*
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.integer.Sign
import org.nexa.threads.millisleep


class BlockchainObjTests
{
    @Test
    fun docObjectExample()
    {
        // This needs to be run before APIs are called, but you should do a single time upon app startup
        initializeLibNexa()

        // This is the Nexa genesis block
        val hdr = blockFromHex(ChainSelector.NEXA, "00000000000000000000000000000000000000000000000000000000000000000000011e0000000000000000000000000000000000000000000000000000000000000000cdafb522e41f4b94618c8ee546f0fab8aaae7057f80e4730ea32df145dec73910000000000000000000000000000000000000000000000000000000000000000c0b2b16200ffffff00000000000000000000000000000000000000000000000000000000007201000000000000010000000403001700010000020000000000000000000100000000000000000000a06a00023b1c4c99526575746572733a204a6170616e20504d204b697368696461206261636b7320424f4a20756c7472612d6561737920706f6c696379207768696c652079656e20776f7272696573206d6f756e74204254433a3734313731313a3030303030303030303030303030303030303037356634626330386531643738613361623361663832373464313333333463306163326465323533303937363800000000")
        println("Height ${hdr.height} Size ${hdr.size} Chain Work ${hdr.chainWork}")
        val tx = hdr.txes[0]
        println("Tx 0 inputs: ${tx.inputs.size}")
        println("Tx 0 outputs: ${tx.outputs.size}")

        // I'm cheating here a little because I know this is the genesis block so I know that the last output
        // of the last transaction is text within an OP_RETURN script.
        val output0Asm = tx.outputs.last().script.toAsm(" ")
        println("tx 0 output 0: $output0Asm")
        val stmts = tx.outputs.last().script.parsed()
        println("\nNexa genesis block says:" + OP.parse(stmts.last()).data!!.decodeUtf8())

        // Create a regtest address object
        val addr = PayAddress("nexareg:nqtsq5g53q3sqyhhp45a86792a7wkkm3gyw9gylhp7kr703l")
    }

    @Test
    fun docWalletExample()
    {
        // This needs to be run before APIs are called, but you should do a single time upon app startup
        initializeLibNexa()

        // This call automatically creates the underlying blockchain object if it hasn't already been created.
        val wal = openOrNewWallet("exampleWallet", ChainSelector.NEXAREGTEST)
        while(!wal.synced())
        {
            // Note that the blockchain may also be syncing headers simultaneously with the wallet syncing, so both
            // numbers may increase.
            //
            // Sync also depends on time -- if the synced block isn't recent enough the wallet does not believe its
            // synced.  So if you are using REGTEST make sure you have recently created a new block before running this test.
            println("syncing ${wal.chainstate?.syncedHeight} of ${wal.blockchain.curHeight}..." )
            millisleep(2000U)
        }

        val newAddress = wal.getNewAddress()
        println("wallet balance is: ${wal.balance}. An address for this wallet is ${newAddress}.")
        try
        {
            // send some coins to myself.  Amounts in this library are ALWAYS in the finest unit.
            wal.send(10000, wal.getNewAddress())
        }
        catch (e: WalletNotEnoughBalanceException)
        {
            if (wal.balance <= 10000) println("You don't have enough coins to actually send.")
        }
    }

    @Test
    fun createATxWithoutAWallet()
    {
        // This needs to be run before APIs are called, but you should do a single time upon app startup
        initializeLibNexa()
        val outpointHex = "put your hex encoded outpoint hash here"
        val secret = UnsecuredSecret("hex encoded 32 bytes".fromHex())
        val outpointAmount = 2000L
        val pto = PayAddress("nexa:pay to this address")
        val s = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, secret,0)
        val fee = 300L

        val script = s.outputScript()
        println("P2PKT script ASM:" + script.toAsm())
        println("pubkey: " + s.pubkeyOrThrow().toHex())
        println("hash160 pubkey: " + libnexa.hash160(s.pubkeyOrThrow()).toHex())
        val scr = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, OP.push(s.pubkeyOrThrow()))
        println("args script: " + scr.toByteArray().toHex() + " or ASM " + scr.toAsm())
        println("hash160 of script pubkey: " + libnexa.hash160(scr.toByteArray()).toHex())
        val tx = txFor(ChainSelector.NEXA)
        val spend = Spendable(ChainSelector.NEXA)
        spend.outpoint = NexaTxOutpoint(outpointHex)
        spend.amount = outpointAmount
        spend.backingPayDestination = s

        val txIn = NexaTxInput(spend)
        txIn.sequence = 4294967294
        //tx.lockTime = locktime
        tx.add(txIn)
        val out = NexaTxOutput(ChainSelector.NEXA)
        out.type = NexaTxOutput.Type.TEMPLATE
        out.script = pto.outputScript()
        out.amount = outpointAmount - fee
        tx.add(out)
        signTransaction(tx)
        val txhex = tx.toHex()
        println("hex tx: " + txhex)
    }

}
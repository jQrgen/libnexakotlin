package org.nexa.libnexakotlin
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsChannel
import io.ktor.client.statement.bodyAsText
import io.ktor.client.statement.request
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import io.ktor.util.cio.toByteArray
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.nexa.threads.*
import kotlin.coroutines.CoroutineContext

expect fun setThreadName(name:String)

public open class Callbacker<T>
{
    protected val mutex = Mutex()
    protected var cnt = 10000
    protected val cbs = mutableMapOf<Int, (T) -> Unit>()

    fun add(fn: (T) -> Unit, key: Int? = null): Int
    {
        return mutex.lock {
            val c: Int = key ?: {
                val t = cnt
                cnt += 1
                t
            }()
            cbs[cnt] = fn
            return@lock c
        }
    }

    fun invoke(param: T)
    {
        val cpy = mutex.lock { cbs.toMap() }
        for (i in cpy.values)
        {
            i(param)
        }
    }

    fun remove(key: Int) = mutex.lock { cbs.remove(key) }
}

public open class Callbacker2<T0,T1>
{
    protected val mutex = Mutex()
    protected var cnt = 10000
    protected val cbs = mutableMapOf<Int, (T0,T1) -> Unit>()

    fun add(fn: (T0,T1) -> Unit, key: Int? = null): Int
    {
        return mutex.lock {

            val c: Int = key ?: {
                val t = cnt
                cnt += 1
                t
            }()
            cbs[cnt] = fn
            c
        }
    }

    fun invoke(param0: T0, param1: T1)
    {
        val cpy = mutex.lock { cbs.toMap() }
        for (i in cpy.values)
        {
            i(param0, param1)
        }
    }

    fun remove(key: Int) = mutex.lock { cbs.remove(key) }
}

public open class StringJoiner(private val delimiter: String)
{
    private val strings = mutableListOf<String>()
    fun add(string: String)
    {
        strings.add(string)
    }

    override fun toString(): String
    {
        return strings.joinToString(delimiter)
    }
}

public fun ByteArray.toHex(): String =
    joinToString("") { it.toUByte().toString(radix = 16).padStart(2, '0') }

@kotlin.ExperimentalUnsignedTypes
public fun UByteArray.toHex(): String =
    joinToString("") { it.toString(radix = 16).padStart(2, '0') }

public fun List<Byte>.toHex(): String =
    joinToString("") { it.toUByte().toString(radix = 16).padStart(2, '0') }

public fun List<UByte>.utoHex(): String =
    joinToString("") { it.toString(radix = 16).padStart(2, '0') }

// implemented now?  fun ByteArray.clone(): ByteArray = copyOf()

public fun Boolean.toInt(): Int = if (this == true) 1 else 0

val HEX_CHARS = "0123456789abcdef"


@kotlin.ExperimentalUnsignedTypes
public fun String.ufromHex(): UByteArray
{
    val result = UByteArray(length / 2)
    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i])
        val secondIndex = HEX_CHARS.indexOf(this[i + 1])
        if ((firstIndex == -1) or (secondIndex == -1)) throw NumberFormatException("invalid hex character")

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toUByte())
    }
    return result
}

// Convert a hex string to a ByteArray
fun String.fromHex(): ByteArray
{

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i])
        val secondIndex = HEX_CHARS.indexOf(this[i + 1])
        if ((firstIndex == -1) or (secondIndex == -1)) throw NumberFormatException("invalid hex character")

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun assert(value: Boolean, message: String = "Assertion failed") {
    if(!value) {
        throw AssertionError(message)
    }
}

fun<T> retry(count: Int, fn:(Int)->T?):T
{
    for(i in 0..count)
    {
        val tmp = fn(i)
        if (tmp != null) return tmp
    }
    throw RetryExceeded()
}

fun String.toByteArray(): ByteArray = encodeToByteArray()

// cut off strings with ... at the end if they are too long
fun String.ellipsis(maxLen: Int): String
{
    if (maxLen <= 0) return ""
    if (maxLen <= 10) return this.take(maxLen)  // If ellipsis would take up a significant amount of the available space, then don't show it.
    if (this.length > maxLen-3) return this.take(maxLen-3) + "..."
    return this
}

fun ByteArray.copyInto(to: ByteArray, location: Int = 0): ByteArray
{
    var pos = location;
    for (i in this)
    {
        to[pos] = i
        pos += 1
    }
    return to
}


// Join a list of ByteArray into a single ByteArray
fun MutableList<ByteArray>.join(): ByteArray
{
    var len = 0
    for (ba in this)
    {
        len += ba.size
    }
    val ret = ByteArray(len)
    len = 0
    for (ba in this)
    {
        for (b in ba)
        {
            ret[len] = b
            len++
        }
    }
    return ret
}

fun<T> synchronized(g: iGate, block:()->T): T
{
    return g.synchronized(block)
}

/** template replacement using a string like "%a foo %b" % mapOf("a" to "first", "b" to "second") returns "first foo second"*/
private val RemKeyEnd = Regex("[^\\w]+")
operator fun String.rem(kv: Map<String, String>): String
{
    val result = StringBuilder()
    val split = this.split("%")
    var first = true
    for (s in split)
    {
        if (first)  // First split is not on a %
        {
            result.append(s)
            first = false
        }
        else
        {
            // Now find the end of the key
            val end = RemKeyEnd.find(s)
            val key = if (end == null) s else s.substring(0, end.range.start)
            val rest = if (end == null) "" else s.substring(end.range.start)
            // Look it up
            if (kv.containsKey(key))
            {
                result.append(kv[key])
            }
            else // Not in this dictionary so restore
            {
                result.append("%")
                result.append(key)
            }
            result.append(rest)
        }
    }
    return result.toString()
}


/** load this Uri (blocking).
 * @return The Uri body contents as a string, and the status code.  If status code 301 or 302 is returned (forwarding) then return the forwarding Uri in the first parameter.
 */
fun io.ktor.http.Url.readText(timeoutInMs: Number, maxReadSize: Number = 250000000, context: CoroutineContext? = null): String
{
    val client = HttpClient() {
        install(HttpTimeout) {
            requestTimeoutMillis = timeoutInMs.toLong()
            // connectTimeoutMillis  // time to connect
            // socketTimeoutMillis   // time between 2 data packets
        }
    }

    var url:io.ktor.http.Url = this
    return runBlocking(context ?: Dispatchers.Default) {
        var tries = 0
        while (tries < 10)
        {
            tries = tries + 1
            val resp: HttpResponse = client.get(url) {
                // Configure request parameters exposed by HttpRequestBuilder
            }
            val status = resp.status.value
            if ((status == 301) or (status == 302))  // Handle URL forwarding (often switching from http to https)
            {
                val newLoc = resp.request.headers.get("Location")
                if (newLoc != null) url = Url(newLoc)
                else throw CannotLoadException(url.toString())
            }
            else return@runBlocking resp.bodyAsText()
        }
        throw CannotLoadException(url.toString())
    }
}

/** This helper function reads the contents of the URL.  This duplicates the API of other URL classes */
fun io.ktor.http.Url.readBytes(timeoutInMs: Number = 30000, maxReadSize: Number = 250000000, context: CoroutineContext? = null): ByteArray
{
    val client = HttpClient() {
        install(HttpTimeout) {
            requestTimeoutMillis = timeoutInMs.toLong()
            connectTimeoutMillis = timeoutInMs.toLong() // time to connect
            // socketTimeoutMillis   // time between 2 data packets
        }
    }

    var url: Url = this
    return runBlocking(context ?: Dispatchers.Default) {
        var tries = 0
        while (tries < 10)
        {
            tries = tries + 1
            try
            {
                val resp: HttpResponse = client.get(url) {
                    // Configure request parameters exposed by HttpRequestBuilder
                }
                val status = resp.status.value
                if ((status == 301) or (status == 302))  // Handle URL forwarding (often switching from http to https)
                {
                    val newLoc = resp.request.headers.get("Location")
                    if (newLoc != null) url = Url(newLoc)
                    else throw CannotLoadException(url.toString())
                    // LogIt.info("forwarded to $url")
                }
                else
                {
                    val chan = resp.bodyAsChannel()
                    val actualBytes = chan.toByteArray(maxReadSize.toInt())
                    if (chan.isClosedForRead) return@runBlocking actualBytes
                    else throw CannotLoadException("Resource is too large at $maxReadSize bytes")
                }
            }
            catch(e: IllegalStateException)
            {
                // IOS limitation: kotlin.IllegalStateException: TLS sessions are not supported on Native platform.
                if (e.message?.contains("TLS sessions") == true)
                {
                    // Note this won't work if your web site just responds with every HTTP request with a forward to HTTPS.  In this case, the code will eventually throw a CannotLoadException
                    val ub = URLBuilder(url)
                    ub.protocol = URLProtocol.HTTP
                    url = ub.build()
                    // LogIt.info("trying $url")
                }
                // else LogIt.error("Cannot access $url, error $e")
            }
            catch(e:Exception)
            {
                // LogIt.error("Cannot access $url, error $e")
                throw e
            }
        }
        throw CannotLoadException(url.toString())
    }
}

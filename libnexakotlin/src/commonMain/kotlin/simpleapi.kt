package org.nexa.libnexakotlin.simpleapi

import org.nexa.libnexakotlin.*

val defaultChain = ChainSelector.NEXA

open class NexaScript(vararg instructions: OP, chainSelector: ChainSelector=defaultChain):
  SatoshiScript(chainSelector,SatoshiScript.Type.SATOSCRIPT, *instructions)
{
    constructor(s:SatoshiScript):this(chainSelector=s.chainSelector)
    {
        data.add(s.flatten())
    }

    fun ungroupedP2t(argsScript : SatoshiScript): SatoshiScript
    {
        return SatoshiScript(chainSelector, SatoshiScript.Type.TEMPLATE, OP.PUSHFALSE, OP.push(this.scriptHash160()), OP.push(argsScript.scriptHash160()))
    }

    fun ungroupedP2t(argsHash:ByteArray?=null): SatoshiScript
    {
        if (argsHash != null)
            return SatoshiScript(chainSelector, SatoshiScript.Type.TEMPLATE, OP.PUSHFALSE, OP.push(this.scriptHash160()), OP.push(argsHash))
        else
            return SatoshiScript(chainSelector, SatoshiScript.Type.TEMPLATE, OP.PUSHFALSE, OP.push(this.scriptHash160()), OP.PUSHFALSE)
    }

    /** Convert this script template into an output script.  Will group (or not) depending on whether groupInfo is null */
    fun constraint(gi:GroupInfo, argsScript:SatoshiScript?=null, visArgs:List<ByteArray>?=null): SatoshiScript
    {
        if (gi.groupId.data.size == 0)
        {
            return ungroupedP2t(argsScript?.scriptHash160())
        }
        // else grouped
        val s = SatoshiScript.grouped(chainSelector, gi.groupId, gi.tokenAmt)
        s.add(OP.push(this.scriptHash160()))
        if ((argsScript == null)||(argsScript.size == 0)) s.add(OP.PUSHFALSE)
        else s.add(OP.push(argsScript.scriptHash160()))

        if (visArgs != null)
           s.add(visArgs)
        return s
    }

    /** Convert this script template into an output script.  Will group (or not) depending on whether groupInfo is null */
    fun constraint(argsScript:SatoshiScript?=null, visArgs:List<ByteArray>?=null): SatoshiScript
    {
        return ungroupedP2t(argsScript?.scriptHash160())
    }

    /** Create an output with this script as the template */
    fun output(amount: Long, args: SatoshiScript?=null,groupInfo:GroupInfo?=null, visArgs:List<ByteArray>?=null): NexaTxOutput
    {
        val outScript = if (groupInfo != null) constraint(groupInfo, args, visArgs) else constraint(args, visArgs)
        return NexaTxOutput(chainSelector, amount, outScript)
    }
}


/** Create an output constraint (script). */
fun NexaConstraint(gid:GroupId, tokenQuantity:ULong, templateHash: ByteArray, argsScript:SatoshiScript?=null, visArgs:List<ByteArray>?=null): SatoshiScript
{
    check(gid.data.size != 0)  // use a different API

    val s = SatoshiScript.grouped(gid.blockchain, gid, tokenQuantity.toLong())
    check(templateHash.size == 20 || templateHash.size == 32)  // template hash must be 20 or 32 bytes
    s.add(OP.push(templateHash))
    if ((argsScript == null)||(argsScript.size == 0)) s.add(OP.PUSHFALSE)
    else s.add(OP.push(argsScript.scriptHash160()))
    if (visArgs != null) s.add(visArgs)
    return s
}

/** Create an output constraint (script). */
fun NexaConstraint(gi:GroupInfo, templateHash: ByteArray, argsScript:SatoshiScript?=null, visArgs:List<ByteArray>?=null): SatoshiScript =
  NexaConstraint(gi.groupId, gi.tokenAmt.toULong(), templateHash, argsScript, visArgs)


fun NexaArgs(vararg args: ByteArray, chainSelector: ChainSelector=defaultChain): SatoshiScript
{
    val ret = SatoshiScript(chainSelector)
    for (a in args)
    {
        ret.add(OP.push(a))
    }
    return ret
}

fun NexaArgs(vararg args: Any, chainSelector: ChainSelector=defaultChain): SatoshiScript
{
    val ret = SatoshiScript(chainSelector)
    for (a in args)
    {
        when (a)
        {
            is Int       -> ret.add(OP.push(a))
            is Long      -> ret.add(OP.push(a))
            is UInt       -> ret.add(OP.push(a))
            is ULong      -> ret.add(OP.push(a))
            is ByteArray -> ret.add(OP.push(a))
        }
    }
    return ret
}


infix fun Long.payTo (s: SatoshiScript): iTxOutput
{
    return NexaTxOutput(defaultChain, this, s)
}
infix fun Long.payTo (s: String): iTxOutput
{
    val p = PayAddress(s)
    return NexaTxOutput(defaultChain, this, p.constraintScript())
}
infix fun Long.payTo (p: PayAddress): iTxOutput
{
    return NexaTxOutput(defaultChain, this, p.constraintScript())
}
infix fun ULong.payTo (s: SatoshiScript): iTxOutput
{
    return NexaTxOutput(defaultChain, this.toLong(), s)
}
infix fun ULong.payTo (s: String): iTxOutput
{
    val p = PayAddress(s)
    return NexaTxOutput(defaultChain, this.toLong(), p.constraintScript())
}
infix fun ULong.payTo (p: PayAddress): iTxOutput
{
    return NexaTxOutput(defaultChain, this.toLong(), p.constraintScript())
}

infix fun Int.payTo (s: SatoshiScript): iTxOutput
{
    return NexaTxOutput(defaultChain, this.toLong(), s)
}

infix fun Int.payTo (s: String): iTxOutput
{
    val p = PayAddress(s)
    return NexaTxOutput(defaultChain, this.toLong(), p.constraintScript())
}

infix fun Int.payTo (p: PayAddress): iTxOutput
{
    return NexaTxOutput(defaultChain, this.toLong(), p.constraintScript())
}

infix fun Int.payTo (p: PayDestination): iTxOutput
{
    return NexaTxOutput(defaultChain, this.toLong(), p.constraintScript())
}

infix fun Int.ofGroup (groupId: GroupId): Pair<ULong, GroupId>
{
    assert(this >= 0)
    return Pair(this.toULong(), groupId)
}
infix fun UInt.ofGroup (groupId: GroupId): Pair<ULong, GroupId> = Pair(this.toULong(), groupId)
infix fun Long.ofGroup (groupId: GroupId): Pair<ULong, GroupId>
{
    assert(this >= 0)
    return Pair(this.toULong(), groupId)
}
infix fun ULong.ofGroup (groupId: GroupId): Pair<ULong, GroupId> = Pair(this, groupId)

infix fun Pair<ULong, GroupId>.payTo (p: PayDestination): iTxOutput
{
    return NexaTxOutput(defaultChain, dust(defaultChain), p.groupedConstraintScript(this.second, this.first.toLong()))
}

infix fun Pair<ULong, GroupId>.payTo (p: PayAddress): iTxOutput
{
    return NexaTxOutput(defaultChain, dust(defaultChain), p.groupedConstraintScript(this.second, this.first.toLong()))
}

infix fun Pair<ULong, GroupId>.payTo (s: String): iTxOutput = this.payTo(PayAddress(s))

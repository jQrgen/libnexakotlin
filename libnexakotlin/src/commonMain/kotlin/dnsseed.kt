// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.nexa.libnexakotlin

// TODO: when we get to API level 29
// import android.net.DnsResolver

fun withPeersFrom(seedername: String, callback: (Set<String>) -> Unit)
{
    callback(setOf<String>(seedername))
}
package org.nexa.libnexakotlin

import java.util.EnumSet
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty1
import kotlin.reflect.full.*
import kotlin.time.DurationUnit
import kotlin.time.TimeSource

val MAX_SHOW_COLLECTION_SIZE = 5

actual val lineSeparator: String = "\n"

actual fun show(p: KProperty1<*, *>, obj: Any): String
{
    if (p.returnType.classifier == MutableMap::class)
    {
        val sz = (p.getter.call(obj) as MutableMap<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableMap<*, *>).size} elements }"
    }
    if (p.returnType.classifier == Map::class)
    {
        val sz = (p.getter.call(obj) as Map<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Map<*, *>).size} elements }"
    }
    if (p.returnType.classifier == MutableList::class)
    {
        try
        {
            val sz = (p.getter.call(obj) as MutableList<*>).size
            if (sz > MAX_SHOW_COLLECTION_SIZE)
                return "[ ${(p.getter.call(obj) as MutableList<*>).size} elements ]"
        }
        catch(e:ClassCastException)  // internal EmptyList can't be cast and can't be checked for
        {
            return "[]"
        }
    }
    if (p.returnType.classifier == List::class)
    {
        try
        {
            val sz = (p.getter.call(obj) as List<*>).size
            if (sz > MAX_SHOW_COLLECTION_SIZE)
                return "[ ${(p.getter.call(obj) as List<*>).size} elements ]"
        }
        catch(e:ClassCastException)  // internal EmptyList can't be cast and can't be checked for
        {
            return "[]"
        }
    }
    if (p.returnType.classifier == MutableSet::class)
    {
        val sz = (p.getter.call(obj) as MutableSet<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableSet<*>).size} elements }"
    }
    if (p.returnType.classifier == Set::class)
    {
        val sz = (p.getter.call(obj) as Set<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Set<*>).size} elements }"
    }

    return p.getter.call(obj).toString()
}


actual fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int, indent: Int): String
{
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"

    val ret = StringBuilder()
    for (p in cls.declaredMemberProperties)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                //val pcls = p.getter.call(obj)!!::class
                val pcls = p.returnType.classifier
                // p.returnType.withNullability(false).findAnnotation<cli>()
                // p.returnType.withNullability(false).classifier!!
                if ((pcls != null) && ((pcls as KClass<*>).findAnnotation<cli>() == null))
                {
                    //val v = (pcls as KClass<*>).declaredMemberExtensionFunctions
                    var value = show(p, obj)
                    if (value.length > 0) value = " = " + value
                    ret.append("  ".repeat(indent) + p.name + showType(p.returnType.toString(), style) + value + showHelp(d.help, style, indent + 1) + fieldSep)
                }
                else
                {
                    p.getter.call(obj)?.let {
                        ret.append("  ".repeat(indent) + p.name + showHelp(d.help, style, indent + 1) + ": " + showObject(it,
                            level,
                            style,
                            depth - 1,
                            indent + 1) + fieldSep)
                    }
                }
            }
        }
    }
    return ret.toString()
}


fun shouldDisplay(obj: KAnnotatedElement, level: Display): Boolean
{
    val d = obj.findAnnotation<cli>() ?: return false
    return (d.display.v <= level.v)
}

fun getClassApis(cls: KClass<*>, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    var uniqueApis = mutableSetOf<String>()
    try
    {
        for (fn in cls.declaredMemberFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            uniqueApis.add(fn.name)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    try
    {
        for (fn in cls.declaredMemberExtensionFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            uniqueApis.add(fn.name)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }
    if (uniqueApis.size == 0) return null
    return uniqueApis.toSortedSet().toList().joinToString(", ")
}

actual fun showApis(obj: Any, level: Display, style: DisplayStyles): String?
{
    val ret = mutableListOf<String>()
    try
    {
        for (st in obj::class.allSuperclasses)
        {
            if (!shouldDisplay(st, level)) continue
            getClassApis(st, level, style)?.let { ret.add(it) }
        }
    }
    catch (e: java.lang.UnsupportedOperationException)
    {

    }
    getClassApis(obj::class, level, style)?.let { ret.add(it) }
    if (ret.size == 0) return null
    return ret.joinToString(", ")
}

fun showFunction(fn: KFunction<*>, level: Display = Display.User, style: DisplayStyles = DEFAULT_STYLE, indent:Int=0): Pair<String?,String>
{
    val sep = "\n"
    val paramSep = "\n"
    val d = fn.findAnnotation<cli>()
    if (d != null)
    {
        if (d.display.v <= level.v)
        {
            val help = showHelp(d.help, style, indent+1, "")
            val s = StringBuilder()
            val pNames = mutableListOf<String>()
            for (p in fn.parameters)
            {
                val dp = p.findAnnotation<cli>()
                s.append("  ".repeat(indent+2) + (p.name ?: "") + showType(p.type.toString(), style) + showHelp(dp?.help, style, indent + 3, ""))
                s.append(paramSep)
                val n:String = if (p.name == null) p.type.toString() else p.name + ":" + p.type
                pNames.add(n)
            }
            val callsig = fn.name + "(" + pNames.joinToString(", ") + ")"
            return Pair(callsig, "  ".repeat(indent) + fn.name + "(" + pNames.joinToString(", ") + ") " + help + sep + s.toString())
        }
    }
    return Pair(null,"")
}

fun getClassApiHelp(cls: KClass<*>, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    val sep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "

    var uniqueApis = sortedMapOf<String, String>()
    try
    {
        for (fn in cls.declaredMemberFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            val (callsig, desc) = showFunction(fn, level, style)
            if (callsig != null) uniqueApis[callsig] = desc
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    try
    {
        for (fn in cls.declaredMemberExtensionFunctions)
        {
            if (!shouldDisplay(fn, level)) continue
            val (callsig, desc) = showFunction(fn, level, style)
            if (callsig != null) uniqueApis[callsig] = desc
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    if (uniqueApis.size == 0) return null
    return uniqueApis.values.joinToString(sep)
}

fun getApiHelp(obj: Any, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    val sep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "

    val ret = mutableListOf<String>()
    try
    {
        for (st in obj::class.allSuperclasses)
        {
            if (!shouldDisplay(st, level)) continue
            getClassApiHelp(st, level, style)?.let { ret.add(it) }
        }
    }
    catch (e: java.lang.UnsupportedOperationException)
    {

    }
    getClassApiHelp(obj::class, level, style)?.let { ret.add(it) }
    if (ret.size == 0) return null
    return ret.joinToString(sep)
}

@OptIn(kotlin.time.ExperimentalTime::class)
actual fun showObject(
    obj: Any,
    level: Display,
    style: DisplayStyles,
    depth: Int,
    indent: Int,
    maxString: Int
): String
{
    if (obj is String) return "\"" + obj + "\""
    if (obj is ByteArray)
    {
        val tmp = obj.toHex().ellipsis(maxString)
        if (tmp.length == 0) return "(empty)"
        return "h" + tmp.ellipsis(maxString)
    }

    if (obj is TimeSource.Monotonic.ValueTimeMark)
    {
        val lastTime = if (obj.elapsedNow().inWholeSeconds < 5*60) obj.elapsedNow().toString(DurationUnit.SECONDS) else obj.elapsedNow().toString(DurationUnit.MINUTES)
        return lastTime + " ago"
    }
    if (isPrimitive(obj))
        return obj.toString()
    var idt = indent
    val ret = StringBuilder()
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"
    // val fieldPerNewLine = if (style.contains(DisplayStyle.FieldPerLine)) "\n" else ""
    val objectSep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "
    val objectOpener = if (style.contains(DisplayStyle.OneLine)) "{" else "\n"
    val objectCloser = if (style.contains(DisplayStyle.OneLine)) "}" else ""

    try
    {
        /*
        for (st in obj::class.allSuperclasses)
        {
            val superProps = showProperties(obj, st, level, style, depth - 1, idt + 1)
            if (superProps.length > 0)  // Don't display the name of a superclass that has no properties of interest
            {
                ret.append("  ".repeat(idt) + st.qualifiedName + fieldSep)
                ret.append(superProps + fieldSep)
            }
        }
         */
        if (obj is Iterable<*>)
        {
            val iter = obj.iterator()
            var i = 0
            ret.append("  ".repeat(idt) + "[" + fieldSep)
            while (iter.hasNext() && i < MAX_SHOW_COLLECTION_SIZE)
            {
                val v = iter.next()
                ret.append(showObject(v ?: throw Exception("Object v is null"), level, style, depth - 1, idt + 1) + fieldSep)
                i++
            }
            if (iter.hasNext()) ret.append("  ".repeat(idt + 1) + "..." + fieldSep)
            ret.append("  ".repeat(idt) + "]" + fieldSep)
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    /* Displays the object's type, but that is now displayed earlier
    val typeName = obj::class.qualifiedName
    if (typeName != null)
    {
        if (style.contains(DisplayStyle.FieldPerLine))
        {
            ret.append("  ".repeat(idt) + "type: " + typeName + fieldSep)
        }
        else
        {
            ret.append(":_ " + typeName.split('.').last())
        }
    } */

    if (style.contains(DisplayStyle.FieldPerLine)) idt += 1
    //if (!style.contains(DisplayStyle.OneLine)) ret.append("\n")

    try
    {
        // Handle annotations in the primary constructor
        obj::class.primaryConstructor?.let { primaryCtor ->
            val vplist: List<KParameter> = primaryCtor.valueParameters
            for (kp in vplist)
            {
                val d = kp.findAnnotation<cli>()
                if (d != null)
                {
                    if (d.display.v <= level.v)
                    {
                        val p = obj::class.memberProperties.find { it.name == kp.name }
                        var value = p?.getter?.call(obj) ?: "null"
                        ret.append(formatForDisplay(style, idt, kp.name ?:"",
                            showType(kp.type.toString(), style),
                            showHelp(d.help, style, indent + 1),
                            showObject(value, level, style, depth - 1, idt + 1) )
                            + objectSep)
                    }
                }
            }
        }

        for (p in obj::class.declaredMemberProperties)
        {
            //println("declaredMemberProperty: " + p.name)
            val d = p.findAnnotation<cli>()
            if (d != null)
            {
                if (d.display.v <= level.v)
                {
                    val subObj = p.getter.call(obj)
                    // decide whether to display details of subobjects
                    if ((subObj != null) && (d.delve + depth > 0))
                        ret.append(formatForDisplay(style, idt, p.name,
                            getObjectType(subObj, style),
                            showHelp(d.help, style, indent + 1),
                            showObject(subObj, level, style, depth - 1, idt + 1)
                        ) + objectSep)
                    // or its summary
                    else ret.append("  ".repeat(idt) + p.name + " = " + subObj + fieldSep)
                }
            }
        }
    }
    catch(e:UnsupportedOperationException)
    {
    }

    if (ret.length > 0)  // If the object has contents, wrap it with {} in one line mode
    {
        ret.append(objectCloser);
        ret.insert(0, objectOpener);
    }
    else  // If the object has nothing, print out its toString representation.  For example, enum classes trigger this clause
    {
        return obj.toString()
    }
    return ret.toString()
}
